# etu
## A time series feature engineering library in C

This repository contains a C library that implements time series feature engineering functions inspired by the Python library [tsfresh](https://tsfresh.readthedocs.io/en/latest/). The library implements many of the feature extraction functions supported in C. It was designed to support [pgetu](https://gitlab.com/tsetu/pgetu) to enable the use of the feature engineering functions in PostgreSQL and its time-series extension TimescaleDB.

In addition to its use in the pgetu Postgres extension, it can be used as a stand-alone library, either directly in C or within Python using [ctypes](https://docs.python.org/3/library/ctypes.html) to load the functions from within the library.

## Requirements

- [GNU Scientific Library](https://www.gnu.org/software/gsl/)
- [cmake](https://cmake.org/) version 3.21

In addition, etu was originally written using the [Intel oneAPI Math Kernel Library](https://www.intel.com/content/www/us/en/developer/tools/oneapi/onemkl.html) and the included C compiler. While this library is no longer needed, the package will take advantage of it when it exists.

## Documentation

- [Wiki](https://gitlab.com/tsetu/etu/-/wikis/home)
- [Feature Calculations](https://gitlab.com/tsetu/etu/-/wikis/functions)

## Installation

Clone etu:

```
git clone https://gitlab.com/tsetu/etu
```

Change to the build directory and run `cmake`:

```
cd build
cmake ..
```

Compile the library with make:

```
make
```

This will create the library, libetu.so. The library and the include file `etu.h` can be installed:

```
make install
```

## Notes

At this time, not all the feature calculations from tsfresh have been implemented. Currently missing are:

- augmented dickey fuller
- agg linear trend
- ar coefficient
- Benford correlation
- change quantiles
- CWT coefficients
- Fourier entropy
- Friedrich coefficients
- Lempel Ziv complexity
- linear trend timewise
- matrix profile
- max langevin fixed point
- permutation entropy
- query similarity count
- spkt Welch density

In addition, not all the functions return the same values as their tsfresh equivalent. tsfresh tends to use biased statistics in its calculations, while etu chooses to use the unbiased version.

