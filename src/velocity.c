#include <time.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double velocity(double array[], int arrayLength, time_t timestamps[]) {
  /**************************************************************************
  Function: velocity
  Return Type: double
  ***************************************************************************
  * Calculate the average velocity over the entire window.
  * This is the change in array from the first to last element
  * divided by the change in time from timestamps.
  **************************************************************************/
  time_t date1, date2;
  double delta, deltadays;
  
  double result;
  
  /* Calculate the delta between the first and last point */
  delta = array[arrayLength-1] - array[0];

  /* Calculate the interval between the first and last date 
   *
   * First get the first and last timestamp. */
  date1 = timestamps[arrayLength-1];
  date2 = timestamps[0];
  
  /* Calculate the difference in days */
  deltadays = difftime(date1, date2) / (60.0*60.0*24.0);

  /* As long as the number of days is larger than zero
   * calculate the velocity */
  if (deltadays > 0) {
    result = delta / deltadays;
  } else {
    result = GSL_NAN;
  }

  return result;
}
