#include <math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double ratio_beyond_r_sigma(double array[], int arrayLength, double r) {
  /**************************************************************************
  Function: ratio_beyond_r_sigma
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of total values that are more than
  * r times the standard deviation from the mean.
  **************************************************************************/
  double mean, std, sum;
  int i;
  double result;

  /* Calculate the mean and standard deviation of the array */
  mean = gsl_stats_mean(array, 1, arrayLength);
  std = gsl_stats_sd(array, 1, arrayLength);

  /* Count the number of times the 
   * absolute value of x-mean is
   * more than r times the 
   * standard deviation */
  sum = 0;
  for (i=0; i<arrayLength; i++) {
    if (fabs(array[i] - mean) > r*std) {
      sum += 1;
    }
  }

  /* Normalize the result by the length of the array */
  result = sum / arrayLength;
  return result;
}
