#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double phi(double array[], int arrayLength, int m, double tolerance) {
  int i, j, k;
  double result;
  result = 0;
  for (i=0; i<arrayLength-m+1; i++) {
    double count = 0;
    for(j=0; j<arrayLength-m+1; j++) {
      double abs_diff = 0;
      for(k=0; k<m; k++) {
	double adiff = fabs(array[i+k] - array[j+k]);
	if (adiff > abs_diff)
	  abs_diff = adiff;
      }
      if (abs_diff <= tolerance)
	count++;
    }
    result += log(count / (arrayLength - m + 1.0));
  }
  return result / (arrayLength - m + 1.0);
}

double approximate_entropy(double array[], int arrayLength,
			   int m, double r) {
  /**************************************************************************
  Function: approximate_entropy
  Return Type: double
  ***************************************************************************
  * Calculate the approximate entropy of an array
  **************************************************************************/
  double std, tolerance, phi1, phi2, result;

  /* Check that r is greater than zero and m is less than the array length */
  if (r <= 0)
    return GSL_NAN;
  
  if (arrayLength < m+1)
    return 0.0;

  /* Calculate Standard Deviation and tolerence */
  std = gsl_stats_sd(array, 1, arrayLength);
  /* NB: Doesn't match tsfresh as tsfresh uses biased estimators */
  /* std = sqrt((arrayLength-1.0)/arrayLength)*std; */

  tolerance = r*std;

  /* Calculate approximate entropy */
  phi1 = phi(array, arrayLength, m, tolerance);

  phi2 = phi(array, arrayLength, m+1, tolerance);

  result = fabs(phi1 - phi2);
  
  return result;
}
