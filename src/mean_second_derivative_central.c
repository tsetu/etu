#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double mean_second_derivative_central(double array[], int arrayLength) {
  /**************************************************************************
  Function: mean_second_derivative_central
  Return Type: double
  ***************************************************************************
  * Calculates the mean of the approximation of the second derivative 
  * between every three points.
  * 
  * NB: This can be simplified to a calculation involving the first and last
  * two points.
  **************************************************************************/
  double numerator, result;

  /* Calculate the numerator as long as the array is longer than 
   * two elements*/
  if (arrayLength > 2) {
    numerator = array[arrayLength-1] - array[arrayLength-2] - array[1] + array[0];
    result = numerator / (2 * (arrayLength - 2));
  } else {
    result = GSL_NAN;
  }

  return result;
}
