#include <gsl/gsl_math.h>

#define TEST_ABSOLUTE_MAXIMUM 39.383
#define TEST_ABSOLUTE_SUM_OF_CHANGES 122.28800000000004
/*#define TEST_AGG_AUTOCORRELATION 0.6897883243294074*/
#define TEST_AGG_AUTOCORRELATION 0.686339
#define TEST_APPROXIMATE_ENTROPY 1.311837078017685
#define TEST_AUTOCORRELATION 0.9357584882903065
#define TEST_BINNED_ENTROPY 1.937690738063906
#define TEST_C3 36796.335199034285
#define TEST_CID_CE 11.918578187015433
#define TEST_COUNT_ABOVE 0.86
#define TEST_COUNT_ABOVE_MEAN 98
#define TEST_COUNT_BELOW 0.14
#define TEST_COUNT_BELOW_MEAN 102
#define TEST_ENERGY 220991.831628
#define TEST_ENERGY_RATIO_BY_CHUNKS 0.09845261441890926
#define TEST_FFT_AGGREGATED 311.24901940944955
#define TEST_FFT_COEFFICIENT -177.58333605725653*M_PI/180.0
#define TEST_FFT_AGGREGATED_CENTROID 6.911179428761034
#define TEST_FFT_AGGREGATED_VARIANCE 311.24901940944955
#define TEST_FFT_AGGREGATED_SKEW 3.1287316671165724
#define TEST_FFT_AGGREGATED_KURTOSIS 12.741182280315522
#define TEST_FFT_COEFFICIENT_REAL -104.3406662191341
#define TEST_FFT_COEFFICIENT_IMAG -4.403569809863294
#define TEST_FFT_COEFFICIENT_ABS 104.43354850871961
#define TEST_FFT_COEFFICIENT_ANGLE -177.58333605725653*M_PI/180.0
#define TEST_FIRST_LOCATION_OF_MAXIMUM 0.96
#define TEST_FIRST_LOCATION_OF_MINIMUM 0.005
#define TEST_HAS_DUPLICATE 1
#define TEST_HAS_DUPLICATE_MAX 0
#define TEST_HAS_DUPLICATE_MIN 0
#define TEST_INDEX_MASS_QUANTILE 0.51
#define TEST_KURTOSIS 0.280280
#define TEST_LARGE_STANDARD_DEVIATION 0
#define TEST_LAST_LOCATION_OF_MAXIMUM 0.965
#define TEST_LAST_LOCATION_OF_MINIMUM 0.010000000000000009
#define TEST_LINEAR_TREND 0.026714542863571594
#define TEST_LINEAR_TREND_SLOPE 0.026714542863571594
#define TEST_LINEAR_TREND_INTERCEPT 30.437002985074628
#define TEST_LONGEST_STRIKE_ABOVE_MEAN 43
#define TEST_LONGEST_STRIKE_BELOW_MEAN 52
#define TEST_MAXIMUM 39.383
#define TEST_MEAN 33.0951
#define TEST_MEAN_ABS_CHANGE 0.6145125628140705
#define TEST_MEAN_CHANGE 0.07400000000000001
#define TEST_MEAN_N_ABSOLUTE_MAX 38.26789999999999
#define TEST_MEAN_SECOND_DERIVATIVE_CENTRAL -0.003366161616161603
#define TEST_MEDIAN 33.035
#define TEST_MINIMUM 22.796
#define TEST_MODE 32.776
#define TEST_NUMBER_CROSSING_M 11
#define TEST_NUMBER_CWT_PEAKS 21
#define TEST_NUMBER_PEAKS 13
#define TEST_NUM_UNIQUE 182
#define TEST_PARTIAL_AUTOCORRELATION 0.9357584882903061
#define TEST_PERCENTAGE_OF_REOCCURRING_DATAPOINTS_TO_ALL_DATAPOINTS 0.175
#define TEST_PERCENTAGE_OF_REOCCURRING_VALUES_TO_ALL_VALUES 0.09340659340659341
#define TEST_QUANTILE 33.035
#define TEST_RANGE_COUNT 50
#define TEST_RATIO_BEYOND_R_SIGMA 0.355
#define TEST_RATIO_VALUE_NUMBER_TO_TIME_SERIES_LENGTH 0.91
#define TEST_ROOT_MEAN_SQUARE 33.24092595190453
#define TEST_SAMPLE_ENTROPY 0.8007778447523107
#define TEST_SKEW -0.426994
#define TEST_STANDARD_DEVIATION 3.118032
/*#define TEST_STANDARD_DEVIATION 3.1102273437805157*/
#define TEST_SUM_OF_REOCCURRING_DATAPOINTS 1195.452
#define TEST_SUM_OF_REOCCURRING_VALUES 581.338
#define TEST_SUM_VALUES 6619.02
#define TEST_SYMMETRY_LOOKING 1
#define TEST_TIME_REVERSAL_ASYMMETRY_STATISTIC 806.1988454083611
#define TEST_VALUE_COUNT 2
#define TEST_VARIANCE 9.722125
/*#define TEST_VARIANCE 9.673514130000001*/
#define TEST_VARIANCE_LARGER_THAN_STANDARD_DEVIATION 1
/*#define TEST_VARIATION_COEFFICIENT 0.09397848454244029*/
#define TEST_VARIATION_COEFFICIENT 0.094214
#define TEST_FIRST 22.886
#define TEST_LAST 37.612
#define TEST_TOTAL_CHANGE (TEST_LAST-TEST_FIRST)
#define TEST_VELOCITY 0.040235
