#include "etu.h"
#include "utils.h"
#include "etumath.h"

double mode(double array[], int arrayLength) {
  /**************************************************************************
  Function: mode
  Return Type: double
  ***************************************************************************
  * Calculate the mode of the time series
  **************************************************************************/
  double mode;

  /* Calculate mode */
  mode = tsmode(array, arrayLength);

  return mode;
}
