#include "etu.h"
#include "utils.h"

int value_count(double array[], int arrayLength, double t) {
  /**************************************************************************
  Function: value_count
  Return Type: double
  ***************************************************************************
  * Calculate the number of values equal to the value t
  **************************************************************************/
  int i, result;

  /* Calculate the number of observations with the value t */
  result = 0;
  for (i=0; i<arrayLength; i++) {
    if (array[i] == t)
      result += 1;
  }

  return result;
}
