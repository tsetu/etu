#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double kurtosis(double array[], int arrayLength) {
  /**************************************************************************
  Function: kurtosis
  Return Type: double
  ***************************************************************************
  * Calculate the kurtosis
  **************************************************************************/
  int i;
  double kurtosis;

  /* Calculate kurtosis */
  /* NB this differs from tsfresh as Python uses 
   * the adjusted Fisher-Pearson standardized moment coefficient */
  kurtosis = gsl_stats_kurtosis(array, 1, arrayLength);

  return kurtosis;
}
