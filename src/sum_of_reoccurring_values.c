#include "etu.h"
#include "utils.h"

double sum_of_reoccurring_values(double array[], int arrayLength) {
  /**************************************************************************
  Function: sum_of_reoccurring_values
  Return Type: double
  ***************************************************************************
  * Calculate the sum of all values that occur more than once in the
  * time series. Include each occurance of the repeated values only once.
  **************************************************************************/
  int i, j, counts;
  double reoccuring_values;

  /* Calculate the number of reoccuring values */
  reoccuring_values = 0;
  for (i=0; i<arrayLength; i++) {
    counts = 1;
    for (j=0; j<arrayLength; j++) {
      if (array[i] == array[j]) {
	if (i < j) {
	  counts++;
	} else if (i > j) {
	  break;
	}
      }
    }
    if (counts > 1) {
      reoccuring_values += array[i];
    }
  }
  
  return reoccuring_values;
}
