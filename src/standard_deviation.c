#include <math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double standard_deviation(double array[], int arrayLength) {
  /**************************************************************************
  Function: standard_deviation
  Return Type: double
  ***************************************************************************
  * Calculate the standard deviation of the time series.
  **************************************************************************/
  double std, result;

  /* Calculate standard deviation */
  std = gsl_stats_sd(array, 1, arrayLength);
  /* std = sqrt((arrayLength-1.0)/arrayLength)*std; */

  return std;
}
