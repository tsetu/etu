#include "etu.h"
#include "utils.h"

double ratio_value_number_to_time_series_length(double array[],
						int arrayLength) {
  /**************************************************************************
  Function: ratio_value_number_to_time_series_length
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of unique values in the time series.
  **************************************************************************/
  int i, j;
  double num_unique, result;

  /* Calculate the number of unique values */
  num_unique = 1;
  for (i=1; i<arrayLength; i++) {
    for (j=0; j<i; j++) {
      if (array[i] == array[j]) {
	break;             
      }
    }
    if (i == j) {
      num_unique++; 
    }
  }

  result = (double)num_unique / arrayLength;

  return result;
}
