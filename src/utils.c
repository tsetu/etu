#include <stdio.h>
#include <string.h>
#include <math.h>
#include <gsl/gsl_block_double.h>
#include <gsl/gsl_vector_double.h>
#include "utils.h"

int doublesortcmp(const void *a, const void *b) {
  if (*(double*)a > *(double*)b)
    return 1;
  else if (*(double*)a < *(double*)b)
    return -1;
  else
    return 0;
}

int reversedoublesortcmp(const void *a, const void *b) {
  if (*(double*)a > *(double*)b)
    return -1;
  else if (*(double*)a < *(double*)b)
    return 1;
  else
    return 0;
}

void create_block(gsl_block *block, double *data, int length) {
  /* Create a GSL block from an existing array */
  (*block).size = length;
  (*block).data = data;
  return;
}

void create_vector_helper(gsl_vector *vector, gsl_block *block, int length) {
  /* Create a GSL vector from and existing array and previously created
   * GSL block */
  (*vector).size = length;
  (*vector).stride = 1;
  (*vector).data = (*block).data;
  (*vector).block = block;
  (*vector).owner = 1;
  return;
}

void create_vector(gsl_vector *vector, gsl_block *block, double *data, int length) {
  /* Create a GSL block and vector from and existing array */
  create_block(block, data, length);
  create_vector_helper(vector, block, length);
  return;
}

void get_digits(int *integral, int *fractional, double value) {
  double ifloor, fround, decimal;
  int pten;

  ifloor = floor(value);
  decimal = value - ifloor;
  pten = 1;
  for(*integral=0; *integral<10; (*integral)++) {
    if (ifloor < pten) break;
    pten *= 10;
  }

  pten = 1;
  for(*fractional=0; *fractional<10; (*fractional)++) {
    fround = round(pten*decimal);
    if (fabs(pten*decimal - fround) < 1e-5) break;
    pten *= 10;
  }
  
  return;
}

void create_fmtstr(char *fmtstr, double value) {
  int integral, fractional, length, negative, ret;

  negative = 0;
  if (fabs(value) != value)
    negative = 1;
  get_digits(&integral, &fractional, fabs(value));

  if ((integral == 0) && (value <= 9) && (value >= -9)) {
    integral = 1;
  }
  if (integral > 0) {
    if (fractional > 0)
      ret = snprintf(fmtstr, FMTSTRSIZE, "%%%d.%df",
			 integral+negative+fractional, fractional);
    else
      ret = snprintf(fmtstr, FMTSTRSIZE, "%%%d.0f", integral+negative);
  } else {
    if (fractional > 0)
      ret = snprintf(fmtstr, FMTSTRSIZE, "%%.%df", fractional);
    else
      fmtstr = "%f";
  }
  return;
}

