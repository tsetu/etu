#include <string.h>
#include <gsl/gsl_block_double.h>
#include <gsl/gsl_vector_double.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double last_location_of_maximum(double array[], int arrayLength) {
  /**************************************************************************
  Function: last_location_of_maximum
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of the whole time series where the maximum
  * value of the series last occurs.
  **************************************************************************/
  gsl_block blk;
  gsl_vector x;
  int argmax;
  double result, data[arrayLength];

  /* Make a copy of array as reversing the vector will overwrite it */
  memcpy(data, array, sizeof(double)*arrayLength);

  /* Create a GSL vector from array */
  create_vector(&x, &blk, data, arrayLength);

  /* reverse vector */
  gsl_vector_reverse(&x);

  /* Get the argmax of the vector */
  argmax = gsl_vector_max_index(&x);

  if (arrayLength > 0)
    result = 1.0 - ((double)argmax / arrayLength);
  else
    result = GSL_NAN;

  return result;
}
