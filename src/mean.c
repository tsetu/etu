#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double mean(double array[], int arrayLength) {
  /**************************************************************************
  Function: mean
  Return Type: double
  ***************************************************************************
  * Calculate the mean of the time series.
  **************************************************************************/
  double mean, result;

  /* Calculate mean */
  mean = gsl_stats_mean(array, 1, arrayLength);

  return mean;
}
