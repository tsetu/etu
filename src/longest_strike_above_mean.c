#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

int longest_strike_above_mean(double array[], int arrayLength) {
  /**************************************************************************
  Function: longest_strike_above_mean
  Return Type: int
  ***************************************************************************
  * Calculate the length of the longest consecutive run of values greater
  * than the mean.
  **************************************************************************/
  double mean;
  int i, strike_above_mean, current_sbm;
  
  /* Calculate mean */
  mean = gsl_stats_mean(array, 1, arrayLength);

  /* Calculate longest strike above mean */
  strike_above_mean = 0;
  current_sbm = 0;

  for (i=0; i<arrayLength; i++) {
    if (array[i] > mean) {
      current_sbm++;
    } else {
      if (current_sbm > strike_above_mean)
	strike_above_mean = current_sbm;
      current_sbm = 0;
    }
  }
  if (current_sbm > strike_above_mean)
    strike_above_mean = current_sbm;

  return strike_above_mean;
}
