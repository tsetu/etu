#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

int has_duplicate_max(double array[], int arrayLength) {
  /**************************************************************************
  Function: has_duplicate_max
  Return Type: int (boolean)
  ***************************************************************************
  * Calculate if the maximum value of the time series occurs more than
  * once.
  **************************************************************************/
  int i, num_max;
  double max;
  int result;
  
  /* Calculate max */
  max = gsl_stats_max(array, 1, arrayLength);
  
  /* Calculate the number of observations with the max value */
  num_max = 0;
  for (i=0; i<arrayLength; i++) {
    if (array[i] == max)
      num_max++;
  }
  
  result = (num_max >= 2);
  
  return result;
}
