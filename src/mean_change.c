#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double mean_change(double array[], int arrayLength) {
  /**************************************************************************
  Function: mean_change
  Return Type: double
  ***************************************************************************
  * Calculates the mean of the differences between consecutive values
  * in the time series.
  *
  * NB this simplifies to simply the difference between the first and
  * last value divided by the length of the series minus 1.
  **************************************************************************/
  double delta, result;

  /* Calculate the delta between the first and last point */
  delta = array[arrayLength-1] - array[0];

  if (arrayLength > 0) {
    result = delta / (arrayLength - 1);
  } else {
    result = GSL_NAN;
  }

  return result;
}
