#include <math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double pair_diff(double array[], int arrayLength, int size, double tolerance) {
  int i, j, k;
  double result;
  result = 0;
  for (i=0; i<arrayLength-size+1; i++) {
    for(j=0; j<arrayLength-size+1; j++) {
      if (i==j)
	continue;
      double abs_diff = 0;
      for(k=0; k<size; k++) {
	double adiff = fabs(array[i+k] - array[j+k]);
	if (adiff > abs_diff)
	  abs_diff = adiff;
      }
      if (abs_diff <= tolerance)
	result++;
    }
  }
  return result;
}

double sample_entropy(double array[], int arrayLength) {
  /**************************************************************************
  Function: sample_entropy
  Return Type: double
  ***************************************************************************
  * Calculates the sample entropy of the time series
  **************************************************************************/
  double std, tolerance, A, B, result;

  /* Calculate Standard Deviation and tolerence */
  std = gsl_stats_sd(array, 1, arrayLength);
  /*std = sqrt((arrayLength-1.0)/arrayLength)*std;*/

  tolerance = 0.2*std;

  /* Calculate B - The count of the number of sequential pairs that 
   * are no more than tolerence away from each other sequential pair */
  B = pair_diff(array, arrayLength, 2, tolerance);

  /* Calculate A - with sequential triplets */
  A = pair_diff(array, arrayLength, 3, tolerance);

  result = -log(A/B);
  return result;
}
