#include <math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

double mean_abs_change(double array[], int arrayLength) {
  /**************************************************************************
  Function: mean_abs_change
  Return Type: double
  ***************************************************************************
  * Calculates the mean of the absolute value of the differences between
  * consecutive values in the array.
  **************************************************************************/
  double x[arrayLength], result;

  /* Calculate the absolute value of the difference between 
   * subsequent values in array*/
  abs_diff(x, array, arrayLength);

  /* Take the mean */
  result = gsl_stats_mean(x, 1, arrayLength-1);
  return result;
}
