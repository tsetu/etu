#include "etu.h"
#include "utils.h"

int has_duplicate(double array[], int arrayLength) {
  /**************************************************************************
  Function: has_duplicate
  Return Type: int (boolean)
  ***************************************************************************
  * Calculate if any value in the time series is duplicated
  **************************************************************************/
  int i, j, num_unique, result;

  /* Calculate the number of unique values */
  result = 1;
  num_unique = 1;
  for (i=1; i<arrayLength; i++) {
    for (j=0; j<i; j++) {
      if (array[i] == array[j]) {
	break;             
      }
    }
    if (i == j) {
      num_unique++; 
    }
  }
  
  result = (arrayLength != num_unique);

  return result;
}
