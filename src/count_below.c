#include "etu.h"
#include "utils.h"

double count_below(double array[], int arrayLength, double t) {
  /**************************************************************************
  Function: count_below
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of values below the value t
  **************************************************************************/
  int i;
  double result;

  /* Calculate the number of observations below t */
  result = 0;
  for (i=0; i<arrayLength; i++) {
    if (array[i] < t)
      result += 1;
  }

  return result/arrayLength;
}
