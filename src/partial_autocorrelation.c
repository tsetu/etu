#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

void partial_autocorrelation(double array[], int arrayLength,
			     int lags[], int lagLength,
			     ETU_RETURN results[]) {
  /**************************************************************************
  Function: partial_autocorrelation
  Return Type: Array of return values
  ***************************************************************************
  * Calculate the partial autocorrelation at the given lag.
  **************************************************************************/
  int i, p, maxlag, pacfsize;
  double result;

  /* Create PACF coefficients */
  maxlag = 0;
  for (i=0; i< lagLength; i++) {
    if (lags[i] > maxlag)
      maxlag = lags[i];
  }

  if (maxlag > 0)
    pacfsize = maxlag;
  else
    pacfsize = 1;
  double pacf_coeffs[pacfsize];

  if (maxlag >= arrayLength/2) {
    maxlag = (arrayLength/2) - 1;
  }
  
  if (arrayLength > 1) {
    pacf(array, arrayLength, maxlag, pacf_coeffs);
  }

  /* Iterate over the lags */
  for (p=0; p<lagLength; p++) {
    double result;
    
    /* Create the key */
    int ret = snprintf(results[p].name, NAME_LENGTH, "lag_%d", lags[p]);

    /* Create the value */
    if (arrayLength <= 1) {
      result = GSL_NAN;
    } else if (maxlag < lags[p]) {
      result = GSL_NAN;
    } else {
      result = pacf_coeffs[lags[p]-1];
    }
    
    results[p].value = result;
  }

}
