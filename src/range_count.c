#include "etu.h"
#include "utils.h"

int range_count(double array[], int arrayLength, double min, double max) {
  /**************************************************************************
  Function: range_count
  Return Type: double
  ***************************************************************************
  * Count the number of values between min and max
  **************************************************************************/
  int i, result;

  /* Calculate the number of observations between min and max */
  result = 0;
  for (i=0; i<arrayLength; i++) {
    if ((array[i] >= min) && (array[i] < max))
      result += 1;
  }

  return result;
}
