#include "etu.h"
#include "utils.h"

double percentage_of_reoccurring_values_to_all_values(double array[],
						      int arrayLength) {
  /**************************************************************************
  Function: percentage_of_reoccurring_values_to_all_values
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of values that occur more than once to
  * the number of distinct values.
  **************************************************************************/
  int i, j, num_distinct, reoccuring_values, counts;
  int counted;
  double result;

  /* Calculate the number of reoccuring values */
  reoccuring_values = 0;
  num_distinct = 0;
  for (i=0; i<arrayLength; i++) {
    counts = 1;
    counted = 1;
    for (j=0; j<arrayLength; j++) {
      if (array[i] == array[j]) {
	if (i < j) {
	  counts++;
	} else if (i > j) {
	  counted = 0;
	  break;
	}
      }
    }
    if (counts > 1) {
      reoccuring_values ++;
    }
    if (counted) {
      num_distinct++;
    }

  }
  
  result = (double)reoccuring_values / num_distinct;
  return result;
}
