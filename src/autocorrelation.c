#include "etu.h"
#include "utils.h"
#include "etumath.h"

double autocorrelation(double array[], int arrayLength,
		       int lag) {
  /**************************************************************************
  Function: autocorrelation
  Return Type: double
  ***************************************************************************
  * Calculates the autocorrelation of the time series for specified lag.
  **************************************************************************/
  double result;
  
  result = autocorr(array, arrayLength, lag);
  return result;
}
