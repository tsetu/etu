#include <gsl/gsl_block_double.h>
#include <gsl/gsl_vector_double.h>

#ifndef FMTSTRSIZE
#define FMTSTRSIZE 32
#endif

void create_block(gsl_block *block, double *data, int length);
void create_vector_helper(gsl_vector *vector, gsl_block *block, int length);
void create_vector(gsl_vector *vector, gsl_block *block, double *data, int length);

int doublesortcmp(const void *a, const void *b);
int reversedoublesortcmp(const void *a, const void *b);

void create_fmtstr(char fmtstr[], double value);
