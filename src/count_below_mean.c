#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

int count_below_mean(double array[], int arrayLength) {
  /**************************************************************************
  Function: count_below_mean
  Return Type: double
  ***************************************************************************
  * Calculate the number of values below the mean
  **************************************************************************/
  double mean;
  int i, result;

  /* Calculate mean */
  mean = gsl_stats_mean(array, 1, arrayLength);

  /* Calculate the number of observations below the mean */
  result = 0;
  for (i=0; i<arrayLength; i++) {
    if (array[i] < mean)
      result += 1;
  }

  return result;
}
