#include <stdio.h>
#include <string.h>
#include <time.h>
#include "etu.h"
#include "etumath.h"
#include "testetu.h"

int main() {
  int arrayLength = 200;
  double array[arrayLength];
  double result;
  int iresult;
  int paramLength = 1;
  ETU_RETURN results[1];

  FILE *ifile = fopen("array.csv", "r");
  fscanf(ifile, "%*[^\n]\n");
  for (int i=0; i<arrayLength; i++) {
    fscanf(ifile, "%lf", &array[i]);
  }
  fclose(ifile);

  /* absolute_maximum */
  printf("absolute_maximum\n");
  result = absolute_maximum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_ABSOLUTE_MAXIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* absolute_sum_of_changes */
  printf("absolute_sum_of_changes\n");
  result = absolute_sum_of_changes(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_ABSOLUTE_SUM_OF_CHANGES))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* agg_autocorrelation */
  printf("agg_autocorrelation\n");
  AGG_AUTOC agg_autoc[1];
  strncpy(agg_autoc[0].f_agg, "mean", NAME_LENGTH);
  agg_autoc[0].maxlag = 10;
  agg_autocorrelation(array, arrayLength, agg_autoc, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_AGG_AUTOCORRELATION))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* approximate_entropy */
  printf("approximate_entropy\n");
  result = approximate_entropy(array, arrayLength, 1, 0.1);
  printf("\t%f\n", result);

  if(isclose(result, TEST_APPROXIMATE_ENTROPY))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* autocorrelation */
  printf("autocorrelation\n");
  result = autocorrelation(array, arrayLength, 1);
  printf("\t%f\n", result);

  if(isclose(result, TEST_AUTOCORRELATION))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* binned_entropy */
  printf("binned_entropy\n");
  result = binned_entropy(array, arrayLength, 10);
  printf("\t%f\n", result);

  if(isclose(result, TEST_BINNED_ENTROPY))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* c3 */
  printf("c3\n");
  result = c3(array, arrayLength, 10);
  printf("\t%f\n", result);

  if(isclose(result, TEST_C3))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* cid_ce */
  printf("cid_ce\n");
  result = cid_ce(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_CID_CE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* count_above */
  printf("count_above\n");
  result = count_above(array, arrayLength, 30.0);
  printf("\t%f\n", result);

  if(result == TEST_COUNT_ABOVE )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* count_above_mean */
  printf("count_above_mean\n");
  iresult = count_above_mean(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_COUNT_ABOVE_MEAN )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* count_below */
  printf("count_below\n");
  result = count_below(array, arrayLength, 30.0);
  printf("\t%f\n", result);

  if(result == TEST_COUNT_BELOW )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* count_below_mean */
  printf("count_below_mean\n");
  iresult = count_below_mean(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_COUNT_BELOW_MEAN )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* energy */
  printf("energy\n");
  result = energy(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_ENERGY))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* energy_ratio_by_chunks */
  printf("energy_ratio_by_chunks\n");
  ERBC erbc[1];
  erbc[0].num_segments = 10;
  erbc[0].segment_focus = 5;
  energy_ratio_by_chunks(array, arrayLength, erbc, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_ENERGY_RATIO_BY_CHUNKS))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* fft_aggregated */
  printf("fft_aggregated - centroid\n");
  FFT_AGG fft_agg[1];
  strncpy(fft_agg[0].aggtype, "centroid", NAME_LENGTH);
  fft_aggregated(array, arrayLength, fft_agg, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_AGGREGATED_CENTROID))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("fft_aggregated - variance\n");
  strncpy(fft_agg[0].aggtype, "variance", NAME_LENGTH);
  fft_aggregated(array, arrayLength, fft_agg, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_AGGREGATED_VARIANCE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("fft_aggregated - skew\n");
  strncpy(fft_agg[0].aggtype, "skew", NAME_LENGTH);
  fft_aggregated(array, arrayLength, fft_agg, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_AGGREGATED_SKEW))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("fft_aggregated - kurtosis\n");
  strncpy(fft_agg[0].aggtype, "kurtosis", NAME_LENGTH);
  fft_aggregated(array, arrayLength, fft_agg, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_AGGREGATED_KURTOSIS))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* fft_coefficient */
  printf("fft_coefficient real\n");
  FFT_COEF fft_coef[1];
  strncpy(fft_coef[0].attr, "real", NAME_LENGTH);
  fft_coef[0].coeff = 1;
  fft_coefficient(array, arrayLength, fft_coef, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_COEFFICIENT_REAL))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("fft_coefficient imag\n");
  strncpy(fft_coef[0].attr, "imag", NAME_LENGTH);
  fft_coef[0].coeff = 1;
  fft_coefficient(array, arrayLength, fft_coef, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_COEFFICIENT_IMAG))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("fft_coefficient abs\n");
  strncpy(fft_coef[0].attr, "abs", NAME_LENGTH);
  fft_coef[0].coeff = 1;
  fft_coefficient(array, arrayLength, fft_coef, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_COEFFICIENT_ABS))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("fft_coefficient angle\n");
  strncpy(fft_coef[0].attr, "angle", NAME_LENGTH);
  fft_coef[0].coeff = 1;
  fft_coefficient(array, arrayLength, fft_coef, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_FFT_COEFFICIENT_ANGLE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* first_location_of_maximum */
  printf("first_location_of_maximum\n");
  result = first_location_of_maximum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_FIRST_LOCATION_OF_MAXIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* first_location_of_minimum */
  printf("first_location_of_minimum\n");
  result = first_location_of_minimum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_FIRST_LOCATION_OF_MINIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* has_duplicate */
  printf("has_duplicate\n");
  iresult = has_duplicate(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_HAS_DUPLICATE )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* has_duplicate_max */
  printf("has_duplicate_max\n");
  iresult = has_duplicate_max(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_HAS_DUPLICATE_MAX )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* has_duplicate_min */
  printf("has_duplicate_min\n");
  iresult = has_duplicate_min(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_HAS_DUPLICATE_MIN )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* index_mass_quantile */
  printf("index_mass_quantile\n");
  double qs[1];
  qs[0] = 0.5;
  index_mass_quantile(array, arrayLength, qs, 1, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_INDEX_MASS_QUANTILE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* kurtosis */
  printf("kurtosis\n");
  result = kurtosis(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_KURTOSIS))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* large_standard_deviation */
  printf("large_standard_deviation\n");
  iresult = large_standard_deviation(array, arrayLength, 1.0);
  printf("\t%d\n", iresult);

  if(iresult == TEST_LARGE_STANDARD_DEVIATION )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* last_location_of_maximum */
  printf("last_location_of_maximum\n");
  result = last_location_of_maximum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_LAST_LOCATION_OF_MAXIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* last_location_of_minimum */
  printf("last_location_of_minimum\n");
  result = last_location_of_minimum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_LAST_LOCATION_OF_MINIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* linear_trend */
  printf("linear_trend - slope\n");
  LT_ATTR lt_attr[1];
  strncpy(lt_attr[0].attr, "slope", NAME_LENGTH);
  linear_trend(array, arrayLength, lt_attr, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_LINEAR_TREND_SLOPE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  printf("linear_trend - intercept\n");
  strncpy(lt_attr[0].attr, "intercept", NAME_LENGTH);
  linear_trend(array, arrayLength, lt_attr, paramLength, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_LINEAR_TREND_INTERCEPT))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* longest_strike_above_mean */
  printf("longest_strike_above_mean\n");
  iresult = longest_strike_above_mean(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_LONGEST_STRIKE_ABOVE_MEAN )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* longest_strike_below_mean */
  printf("longest_strike_below_mean\n");
  iresult = longest_strike_below_mean(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_LONGEST_STRIKE_BELOW_MEAN )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* maximum */
  printf("maximum\n");
  result = maximum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MAXIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* mean */
  printf("mean\n");
  result = mean(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MEAN))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* mean_abs_change */
  printf("mean_abs_change\n");
  result = mean_abs_change(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MEAN_ABS_CHANGE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* mean_change */
  printf("mean_change\n");
  result = mean_change(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MEAN_CHANGE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* mean_n_absolute_max */
  printf("mean_n_absolute_max\n");
  result = mean_n_absolute_max(array, arrayLength, 10);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MEAN_N_ABSOLUTE_MAX))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* mean_second_derivative_central */
  printf("mean_second_derivative_central\n");
  result = mean_second_derivative_central(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MEAN_SECOND_DERIVATIVE_CENTRAL))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* median */
  printf("median\n");
  result = median(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MEDIAN))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* minimum */
  printf("minimum\n");
  result = minimum(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MINIMUM))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* mode */
  printf("mode\n");
  result = mode(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_MODE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* number_crossing_m */
  printf("number_crossing_m\n");
  iresult = number_crossing_m(array, arrayLength, 34);
  printf("\t%d\n", iresult);

  if(iresult == TEST_NUMBER_CROSSING_M )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* number_cwt_peaks */
  printf("number_cwt_peaks\n");
  iresult = number_cwt_peaks(array, arrayLength, 5);
  printf("\t%d\n", iresult);

  if(iresult == TEST_NUMBER_CWT_PEAKS )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* number_peaks */
  printf("number_peaks\n");
  iresult = number_peaks(array, arrayLength, 5);
  printf("\t%d\n", iresult);

  if(iresult == TEST_NUMBER_PEAKS )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* num_unique */
  printf("num_unique\n");
  iresult = num_unique(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_NUM_UNIQUE )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* partial_autocorrelation */
  printf("partial_autocorrelation\n");
  int lags[1];
  lags[0] = 1;
  partial_autocorrelation(array, arrayLength, lags, 1, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_PARTIAL_AUTOCORRELATION))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* percentage_of_reoccurring_datapoints_to_all_datapoints */
  printf("percentage_of_reoccurring_datapoints_to_all_datapoints\n");
  result = percentage_of_reoccurring_datapoints_to_all_datapoints(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_PERCENTAGE_OF_REOCCURRING_DATAPOINTS_TO_ALL_DATAPOINTS))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* percentage_of_reoccurring_values_to_all_values */
  printf("percentage_of_reoccurring_values_to_all_values\n");
  result = percentage_of_reoccurring_values_to_all_values(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_PERCENTAGE_OF_REOCCURRING_VALUES_TO_ALL_VALUES))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* quantile */
  printf("quantile\n");
  result = quantile(array, arrayLength, 0.5);
  printf("\t%f\n", result);

  if(isclose(result, TEST_QUANTILE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* range_count */
  printf("range_count\n");
  iresult = range_count(array, arrayLength, 30.0, 32.0);
  printf("\t%d\n", iresult);

  if(iresult == TEST_RANGE_COUNT)
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* ratio_beyond_r_sigma */
  printf("ratio_beyond_r_sigma\n");
  result = ratio_beyond_r_sigma(array, arrayLength, 1.0);
  printf("\t%f\n", result);

  if(isclose(result, TEST_RATIO_BEYOND_R_SIGMA))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* ratio_value_number_to_time_series_length */
  printf("ratio_value_number_to_time_series_length\n");
  result = ratio_value_number_to_time_series_length(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_RATIO_VALUE_NUMBER_TO_TIME_SERIES_LENGTH))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* root_mean_square */
  printf("root_mean_square\n");
  result = root_mean_square(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_ROOT_MEAN_SQUARE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* sample_entropy */
  printf("sample_entropy\n");
  result = sample_entropy(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_SAMPLE_ENTROPY))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* skew */
  printf("skew\n");
  result = skew(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_SKEW))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* standard_deviation */
  printf("standard_deviation\n");
  result = standard_deviation(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_STANDARD_DEVIATION))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* sum_of_reoccurring_datapoints */
  printf("sum_of_reoccurring_datapoints\n");
  result = sum_of_reoccurring_datapoints(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_SUM_OF_REOCCURRING_DATAPOINTS))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* sum_of_reoccurring_values */
  printf("sum_of_reoccurring_values\n");
  result = sum_of_reoccurring_values(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_SUM_OF_REOCCURRING_VALUES))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* sum_values */
  printf("sum_values\n");
  result = sum_values(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_SUM_VALUES))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* symmetry_looking */
  printf("symmetry_looking\n");
  double rs[1];
  rs[0] = 0.5;
  symmetry_looking(array, arrayLength, rs, 1, results);
  printf("\t%s: %f\n", results[0].name, results[0].value);

  if(isclose(results[0].value, TEST_SYMMETRY_LOOKING))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* time_reversal_asymmetry_statistic */
  printf("time_reversal_asymmetry_statistic\n");
  result = time_reversal_asymmetry_statistic(array, arrayLength, 3);
  printf("\t%f\n", result);

  if(isclose(result, TEST_TIME_REVERSAL_ASYMMETRY_STATISTIC))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* total_change */
  printf("total_change\n");
  result = total_change(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_TOTAL_CHANGE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* first */
  printf("first\n");
  result = first(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_FIRST))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* last */
  printf("last\n");
  result = last(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_LAST))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* value_count */
  printf("value_count\n");
  iresult = value_count(array, arrayLength, 37.592);
  printf("\t%d\n", iresult);

  if(iresult == TEST_VALUE_COUNT)
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* variance */
  printf("variance\n");
  result = variance(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_VARIANCE))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* variance_larger_than_standard_deviation */
  printf("variance_larger_than_standard_deviation\n");
  iresult = variance_larger_than_standard_deviation(array, arrayLength);
  printf("\t%d\n", iresult);

  if(iresult == TEST_VARIANCE_LARGER_THAN_STANDARD_DEVIATION )
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* variation_coefficient */
  printf("variation_coefficient\n");
  result = variation_coefficient(array, arrayLength);
  printf("\t%f\n", result);

  if(isclose(result, TEST_VARIATION_COEFFICIENT))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  /* velocity */
  printf("velocity\n");

  time_t timestamps[arrayLength];
  struct tm breakdown = {0};
  breakdown.tm_year = 116;
  breakdown.tm_mon = 0;
  breakdown.tm_mday = 1;
  breakdown.tm_hour = 0;
  breakdown.tm_min = 0;
  for (int i=0; i<arrayLength; i++) {
    timestamps[i] = mktime(&breakdown);
  }
  breakdown.tm_year = 117;
  timestamps[arrayLength-1] = mktime(&breakdown);

  result = velocity(array, arrayLength, timestamps);
  printf("\t%f\n", result);

  if(isclose(result, TEST_VELOCITY))
    printf("Matched\n");
  else
    printf("Failed\n");

  printf("================================================\n\n");

  return 0;
}
