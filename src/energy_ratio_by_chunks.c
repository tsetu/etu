#include <gsl/gsl_math.h>
#include <math.h>
#include "etu.h"
#include "utils.h"

int energy_ratio_by_chunks(double array[], int arrayLength,
			   ERBC params[], int paramLength,
			     ETU_RETURN results[]) {
  /**************************************************************************
  Function: energy_ratio_by_chunks
  Return Type: Array of return values
  ***************************************************************************
  * TODO: write explanation
  **************************************************************************/
  double energy;
  int i;

  /* Calculate series energy */
  energy = 0;
  for (i=0; i<arrayLength; i++) {
    energy += array[i]*array[i];
  }

  /* Iterate over the params, create the calculation 
   * and save it in the return object */
  for (i=0; i<paramLength; i++) {
    int num_seg, seg_focus;
    int j, long_seg, length1, length2, start_index, end_index;
    
    double segment_energy, result;
    
    num_seg = params[i].num_segments;
    seg_focus = params[i].segment_focus;

    /* Both num_seg and seg_focus must be > 0 and
     * seg_focus must be smaller than num_seg */
    if (num_seg <= 0) {
      return 1;
    } else if (seg_focus < 0) {
      return 1;
    } else if (seg_focus >= num_seg) {
      return 1;
    }
    
    /* Create the key */
    int ret = snprintf(results[i].name, NAME_LENGTH,
		       "num_segments_%d_segment_focus_%d",
		       num_seg, seg_focus);

    /* Create the value */
    /* Calculate how many arrays of size arrayLength // n + 1*/
    long_seg = arrayLength % num_seg;

    /* Calculate the size of the long and short segments */
    length1 = (arrayLength / num_seg) + 1;
    length2 = arrayLength / num_seg;
    

    if (energy == 0) {
      result = GSL_NAN;
    } else if (arrayLength == 0) {
      result = GSL_NAN;
    } else if (arrayLength == 1) {
      result = 1.0;
    } else {
      /* Calculate start and end indices for segment */
      /* Note seg_focus starts with 0 */
      if (num_seg > arrayLength) {
	/* If array is shorter than number of segments, use length 1 */
	//ereport(ERROR, (errmsg("num_segment greater than array length")));
	if (seg_focus < arrayLength) {
	  start_index = seg_focus;
	  end_index = start_index + 1;
	}
      } else if (seg_focus < long_seg) {
	//start_index = (seg_focus - 1) * length1;
	start_index = seg_focus * length1;
	end_index = start_index + length1;
      } else {
	//start_index = long_seg*length1 + (seg_focus-long_seg-1)*length2;
	start_index = long_seg*length1 + (seg_focus-long_seg)*length2;
	end_index = start_index + length2;
      }
      
      /* Calculate the segment energy */
      segment_energy = 0;
      if (seg_focus >= arrayLength) {
	/* Correct for values beyond end of array */
      } else {
	for (j=start_index ; j<end_index ;j++) {
	  segment_energy += array[j]*array[j];
	}
      }
      
      result = segment_energy/energy;
    }

    results[i].value = result;
  }

  return 0;
}
