#include "etu.h"
#include "utils.h"

double total_change(double array[], int arrayLength) {
  /**************************************************************************
  Function: total_change
  Return Type: double
  ***************************************************************************
  * Calculate the difference between the last and the first value
  * of the time series.
  **************************************************************************/
  double result;

  /* Calculate the delta between the first and last point */
  result = array[arrayLength-1] - array[0];

  return result;
}
