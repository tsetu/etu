#include "etu_intel.h"
#if defined USE_INTEL
#include <mkl.h>
#else
#include <string.h>
#include <gsl/gsl_block_double.h>
#include <gsl/gsl_vector_double.h>
#include <gsl/gsl_blas.h>
#endif
#include <math.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

double cid_ce(double array[], int arrayLength) {
  /**************************************************************************
  Function: cid_ce
  Return Type: double
  ***************************************************************************
  * Calculates a measure of time series complexity using a 
  * complexity-invariant distance
  **************************************************************************/
  double data[arrayLength], result;

  /* Calculate the difference between subsequent elements in
   * array */
  diff(data, array, arrayLength);
  /* Calculate the square root of the sum of the squares
   * of the difference (dot product) */
#if defined USE_INTEL
  result = sqrt(cblas_ddot(arrayLength-1, data, 1, data, 1));
#else
  gsl_block blk;
  gsl_vector x;

  create_vector(&x, &blk, data, arrayLength-1);
  gsl_blas_ddot(&x, &x, &result);
  result = sqrt(result);
#endif
  return result;
}
