#include <string.h>
#include <math.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

void fft_coefficient(double array[], int arrayLength,
		     FFT_COEF params[], int paramLength,
		     ETU_RETURN results[]) {
  /**************************************************************************
  Function: fft_coefficient
  Return Type: array of return values
  ***************************************************************************
  * Calculates the FFT of the time series and returns the coefficients
  * of the time series. This could be the real or imaginary part
  * or the angle and magnitude.
  **************************************************************************/
  int c;
  double realCoeffs[arrayLength], imagCoeffs[arrayLength], data[arrayLength];

  /* Make a copy of array as FFT overwrites it */
  memcpy(data, array, sizeof(double)*arrayLength);
    
  /* Calculate the FFT Coeff */
  calculate_fft_coeff(data, arrayLength, realCoeffs, imagCoeffs);
  
  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (c=0; c<paramLength; c++) {
    double result;
    
    /* Create the key */
    int ret = snprintf(results[c].name, NAME_LENGTH, "attr_%s_coeff_%d",
	     params[c].attr, params[c].coeff);

    /* Create the value */
    if (arrayLength < params[c].coeff) {
      result = GSL_NAN;
    } else if (!strncmp(params[c].attr, "real", 32)) {
      result = realCoeffs[params[c].coeff];
    } else if (!strncmp(params[c].attr, "imag", 32)) {
      result = imagCoeffs[params[c].coeff];
    } else if (!strncmp(params[c].attr, "abs", 32)) {
      result = sqrt(realCoeffs[params[c].coeff]*realCoeffs[params[c].coeff]
		    + imagCoeffs[params[c].coeff]*imagCoeffs[params[c].coeff]);
    } else if (!strncmp(params[c].attr, "angle", 32)) {
      result = atan2(imagCoeffs[params[c].coeff], realCoeffs[params[c].coeff]);
    } else {
      result = GSL_NAN;
    }
    results[c].value = result;
  }

}
