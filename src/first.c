#include "etu.h"
#include "utils.h"

double first(double array[], int arrayLength) {
  /**************************************************************************
  Function: first
  Return Type: double
  ***************************************************************************
  * Return the first or earliest value in the time series.
  **************************************************************************/

  return array[0];
}
