#include <string.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

void fft_aggregated(double array[], int arrayLength,
		    FFT_AGG params[], int paramLength,
		    ETU_RETURN results[]) {
  /**************************************************************************
  Function: fft_aggregated
  Return Type: feature_return
  ***************************************************************************
  * Calculate the FFT of the time series and return the requested 
  * aggregates: centroid, variance, skew or kurtosis.
  **************************************************************************/
  int c, maxlength;
  double data[arrayLength];

  /* Make a copy of array as FFT overwrites it */
  memcpy(data, array, sizeof(double)*arrayLength);
  
  /* Calculate the Absolute Value of FFT Coeff */
  maxlength = arrayLength/2 + 1;
  double abs[maxlength];
  calc_fft_abs(data, arrayLength, abs, maxlength);

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (c=0; c<paramLength; c++) {
    double result;
    
    /* Create the key */
    int ret = snprintf(results[c].name, NAME_LENGTH, "aggtype_%s",
		       params[c].aggtype);

    /* Create the value */
    if (!strncmp(params[c].aggtype, "centroid", 32)) {
      result = fft_centroid(abs, maxlength);
      results[c].value = result;
    } else if (!strncmp(params[c].aggtype, "variance", 32)) {
      result = fft_variance(abs, maxlength);
      results[c].value = result;
    } else if (!strncmp(params[c].aggtype, "skew", 32)) {
      result = fft_skew(abs, maxlength);
      results[c].value = result;
    } else if (!strncmp(params[c].aggtype, "kurtosis", 32)) {
      result = fft_kurtosis(abs, maxlength);
      results[c].value = result;
    } else {
      results[c].value = GSL_NAN;
    }

  }

}
