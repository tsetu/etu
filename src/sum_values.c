#include "etu.h"
#include "etumath.h"

double sum_values(double array[], int arrayLength) {
  /**************************************************************************
  Function: sum_values
  Return Type: double
  ***************************************************************************
  * Calculates the sum of the values of the time series
  **************************************************************************/
  double result;

  /* Calculate the sum */
  result = KahanSum(array, arrayLength);
 
  return result;
}
