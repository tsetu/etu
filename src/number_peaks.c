#include "etu.h"
#include "utils.h"

int number_peaks(double array[], int arrayLength, int n) {
  /**************************************************************************
  Function: number_peaks
  Return Type: int
  ***************************************************************************
  * Calculates the number of peaks which are supported by lower values
  * of at least n on both sides.
  **************************************************************************/
  int peaks;
  int supported_peak;
  int i, j;
 
  /* Calculate number of peaks */
  peaks = 0;
  for (i=n; i < arrayLength-n; i++) {
    supported_peak = 1;
    for (j=i-n; j <= i+n; j++) {
      if ((i!=j) && (array[j] >= array[i])) {
	/* Not supported */
	supported_peak = 0;
	break;
      }
    }
    if (supported_peak)
      peaks += 1;
  }

  return peaks;
}
