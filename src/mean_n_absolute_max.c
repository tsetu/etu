#include <string.h>
#include <math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double mean_n_absolute_max(double array[], int arrayLength, int n) {
  /**************************************************************************
  Function: mean_n_absolute_max
  Return Type: double
  ***************************************************************************
  * Calculate the mean of the top n absolute values of the time series.
  **************************************************************************/
  double abs_x[arrayLength], sorted[arrayLength];
  double mean, result;
  int i;

  /* Calculate the absolute value of the time series */
  for (i=0; i<arrayLength; i++) {
    abs_x[i] = fabs(array[i]);
  }

  
  /* Sort the absolute values */
  memcpy(sorted, array, arrayLength*sizeof(double));
  qsort(sorted, arrayLength, sizeof(double), reversedoublesortcmp);
  
  mean = gsl_stats_mean(sorted, 1, n);

  return mean;
}
