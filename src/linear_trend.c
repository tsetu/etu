#include <string.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

void linear_trend(double array[], int arrayLength,
		  LT_ATTR attrs[], int attrLength,
		  ETU_RETURN results[]) {
  /**************************************************************************
  Function: linear_trend
  Return Type: Array of return values
  ***************************************************************************
  * Calculate the linear regression and return its slope, intercept
  * or statistics.
  *
  * TODO: fix statistics to work.
  **************************************************************************/
  int c;
  double slope, intercept, pvalue, rvalue, stderr;
  
  /* Calculate the linear regression*/
  if (arrayLength > 1) {
    regression(array, arrayLength, &slope, &intercept, &pvalue, &rvalue, &stderr);
  } else {
    slope = GSL_NAN;
    intercept = GSL_NAN;
    pvalue = GSL_NAN;
    rvalue = GSL_NAN;
    stderr = GSL_NAN;
  }

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (c=0; c<attrLength; c++) {
    double result;
    
    /* Create the key */
    int ret = snprintf(results[c].name, NAME_LENGTH, "attr_%s", attrs[c].attr);

    /* Create the value */
    if (!strncmp(attrs[c].attr, "slope", 32)) {
      result = slope;
    } else if (!strncmp(attrs[c].attr, "intercept", 32)) {
      result = intercept;
    } else if (!strncmp(attrs[c].attr, "pvalue", 32)) {
      result = pvalue;
    } else if (!strncmp(attrs[c].attr, "rvalue", 32)) {
      result = rvalue;
    } else if (!strncmp(attrs[c].attr, "stderr", 32)) {
      result = stderr;
    } else {
      result = GSL_NAN;
    }

    results[c].value = result;
  }

}
