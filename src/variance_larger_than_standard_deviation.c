#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

int variance_larger_than_standard_deviation(double array[], int arrayLength) {
  /**************************************************************************
  Function: variance_larger_than_standard_deviation
  Return Type: integer (boolean)
  ***************************************************************************
  * Calculate if the variance is larger than the standard deviation
  **************************************************************************/
  double var, sd;
  int result;
  
  /* Calculate variance and standard deviation */
  var = gsl_stats_variance(array, 1, arrayLength);
  sd = gsl_stats_sd(array, 1, arrayLength);
  
  result = (var > sd);
  
  return result;
}
