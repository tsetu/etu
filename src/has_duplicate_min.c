#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

int has_duplicate_min(double array[], int arrayLength) {
  /**************************************************************************
  Function: has_duplicate_min
  Return Type: int (boolean)
  ***************************************************************************
  * Calculate if the minimum value of the time series occurs more than
  * once.
  **************************************************************************/
  int i, num_min;
  double min;
  int result;
  
  /* Calculate min */
  min = gsl_stats_min(array, 1, arrayLength);
  
  /* Calculate the number of observations with the min value */
  num_min = 0;
  for (i=0; i<arrayLength; i++) {
    if (array[i] == min)
      num_min++;
  }
  
  result = (num_min >= 2);
  
  return result;
}
