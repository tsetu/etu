#include "etu.h"
#include "utils.h"

double count_above(double array[], int arrayLength, double t) {
  /**************************************************************************
  Function: count_above
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of values above the value t
  **************************************************************************/
  int i;
  double result;

  /* Calculate the number of observations over the value t */
  result = 0;
  for (i=0; i<arrayLength; i++) {
    if (array[i] > t)
      result += 1;
  }

  return result/arrayLength;
}
