#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double first_location_of_maximum(double array[], int arrayLength) {
  /**************************************************************************
  Function: first_location_of_maximum
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of the whole time series where the maximum
  * value of the series first occurs.
  **************************************************************************/
  gsl_block blk;
  gsl_vector x;
  int argmax;
  double result;

  /* Get the argmax of the vector */
  argmax = gsl_stats_max_index(array, 1, arrayLength);

  if (arrayLength > 0)
    result = (double)argmax / arrayLength;
  else
    return GSL_NAN;

  return result;
}
