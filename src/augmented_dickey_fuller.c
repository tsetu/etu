#include "etu.h"
#include "utils.h"

void augmented_dickey_fuller(double array[], int arrayLength,
			     AUG_DF params[], int paramLength,
			     ETU_RETURN results[]) {
  /**************************************************************************
  Function: augmented_dickey_fuller
  Return Type: array of return values
  ***************************************************************************
  * Calculate the augmented Dickey-Fuller test to check if the time series
  * is stationary
  *
  * NB Not implemented yet
  * TODO: implementation
  **************************************************************************/
  int a;
  double result;
  
  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (a=0; a<paramLength; a++) {
    
    /* Create the key */
    int ret = snprintf(results[a].name, NAME_LENGTH, "attr_%s_autolag_%s", params[a].attr, params[a].autolag);

    /* Create the value */
    result = 0.0;
    results[a].value = result;
  }

}
