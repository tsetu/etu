#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

int large_standard_deviation(double array[], int arrayLength,
			     double r) {
  /**************************************************************************
  Function: large_standard_deviation
  Return Type: integer (boolean)
  ***************************************************************************
  * Calculate if the standard deviation is larger than r times the
  * range of the entire time series.
  **************************************************************************/
  double min, max, std, result;

  /* Calculate the necessary stats */
  min = gsl_stats_min(array, 1, arrayLength);
  max = gsl_stats_max(array, 1, arrayLength);
  std = gsl_stats_sd(array, 1, arrayLength);

  if (std > (r*(max - min))) {
    result = 1;
  } else {
    result = 0;
  }

  return result;
}
