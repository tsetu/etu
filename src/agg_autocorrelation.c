#include <string.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

void agg_autocorrelation(double array[], int arrayLength,
			 AGG_AUTOC params[], int paramLength,
			 ETU_RETURN results[]) {
  /**************************************************************************
  Function: agg_autocorrelation
  Return Type: array of return values
  ***************************************************************************
  * Compute autocorrelation function and calculate requested aggregations
  **************************************************************************/
  int f, i, j, max_lag;
  double mean, var;

  /* Calculate the mean and variance */
  mean = gsl_stats_mean(array, 1, arrayLength);
  var = gsl_stats_variance(array, 1, arrayLength);
  /* NB: tsfresh uses biased calc for var */
  /* var *= (arrayLength - 1.0)/arrayLength; */

  /* Create the autocorrelation function to max length */
  max_lag = 0;
  for (i=0; i<paramLength; i++) {
    if (params[i].maxlag > max_lag)
      max_lag = params[i].maxlag;
  }
  if (arrayLength < max_lag)
    max_lag = arrayLength - 1;

  double acf[max_lag];

  if ((fabs(var) < 1e-10) || (arrayLength == 1)) {
    for (i=0; i<max_lag; i++) {
      acf[i] = 0;
    }
  } else {
    for (i=0; i<max_lag; i++) {
      acf[i] = 0;
      for (j=0; j<arrayLength-i-1; j++) {
	acf[i] += (array[j]-mean)*(array[j+i+1]-mean);
      }
      acf[i] /= ((arrayLength-i-1)*var);
    }
  }

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (f=0; f<paramLength; f++) {
    int maxlag;
    
    /* Create the return name */
    int ret = snprintf(results[f].name, NAME_LENGTH, "f_agg_%s_maxlag_%d", params[f].f_agg, params[f].maxlag);

    /* Create the value */
    maxlag = params[f].maxlag;
    if (arrayLength <= maxlag)
      maxlag = arrayLength-1;

    if (!strncmp(params[f].f_agg, "mean", 32)) {
      results[f].value = gsl_stats_mean(acf, 1, maxlag);
    } else if (!strncmp(params[f].f_agg, "var", 32)) {
      results[f].value = gsl_stats_variance(acf, 1, maxlag);
      /* Fixes to match numpy
      results[f].value *= (maxlag-1.0)/maxlag;
      if (maxlag == 1)
	results[f].value = 0;
      */
    } else if (!strncmp(params[f].f_agg, "std", 32)) {
      results[f].value = gsl_stats_sd(acf, 1, maxlag);
      //results[f].value *= sqrt((maxlag-1.0)/maxlag);
    } else if (!strncmp(params[f].f_agg,  "median", 32)) {
      qsort(acf, maxlag, sizeof(double), doublesortcmp);
      results[f].value = gsl_stats_median_from_sorted_data(acf, 1, maxlag);
    } else {
      results[f].value = GSL_NAN;
    }

  }

}
