#include "etu.h"
#include "utils.h"
#include "etumath.h"

int number_crossing_m(double array[], int arrayLength, double m) {
  /**************************************************************************
  Function: number_crossing_m
  Return Type: integer
  ***************************************************************************
  * Calculates the number of times the time series crosses m. This is 
  * sequential values where the first is below m and the senond is above
  * or vice versa.
  **************************************************************************/
  double crosses;
 
  /* Calculate then number of times the array crosses m */
  crosses = crossing(array, arrayLength, m);
  
  return crosses;
}
