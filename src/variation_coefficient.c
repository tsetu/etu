#include <gsl/gsl_math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double variation_coefficient(double array[], int arrayLength) {
  /**************************************************************************
  Function: variation_coefficient
  Input Type: REAL[]
  Return Type: REAL
  ***************************************************************************
  * Calculate the variation coefficient or standard deviation divided
  * by the mean.
  **************************************************************************/
  double mean, sd, result;

  /* Calculate mean and standard deviation */
  mean = gsl_stats_mean(array, 1, arrayLength);
  sd = gsl_stats_sd(array, 1, arrayLength);

  if (mean == 0) {
    result = GSL_NAN;
  } else {
    result = sd / mean;
  }

  return result;
}
