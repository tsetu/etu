#include "etu.h"
#include "utils.h"

int num_unique(double array[], int arrayLength) {
  /**************************************************************************
  Function: num_unique
  Return Type: int
  ***************************************************************************
  * Calculate the number of unique values in the time series.
  **************************************************************************/
  int i, j, num_unique;

  /* Calculate the number of unique values */
  num_unique = 1;
  for (i=1; i<arrayLength; i++) {
    for (j=0; j<i; j++) {
      if (array[i] == array[j]) {
	break;             
      }
    }
    if (i == j) {
      num_unique++; 
    }
  }
  
  return num_unique;
}
