#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

void index_mass_quantile(double array[], int arrayLength,
			 double qs[], int qLength,
			 ETU_RETURN results[]) {
  /**************************************************************************
  Function: index_mass_quantile
  Return Type: feature_return
  ***************************************************************************
  * Calculates the percentage of the time series from the beginning
  * that contains q% of the total value in the entire series.
  **************************************************************************/
  double abs_x[arrayLength];
  double sum, cummulative, result;
  int i, q;

  /* Calculate absolute array and sum */
  sum = 0;
  for(i=0; i<arrayLength; i++) {
    abs_x[i] = fabs(array[i]);
    sum += abs_x[i];
  }

  /* Iterate over the parameters, create the calculation 
   * and save it in the JSONB object */
  for (q=0; q<qLength; q++) {
    int argmax;
    char fmtstr[FMTSTRSIZE];

    /* Create the key */
    create_fmtstr(fmtstr, qs[q]);
    int ret = snprintf(results[q].name, NAME_LENGTH, fmtstr, qs[q]);
    
    /* Create the value */
    /* Calculate count of mass_centralized >= parameter*/
    if (sum == 0) {
      result = GSL_NAN;
    } else {
      cummulative = 0;
      argmax = 0;
      for(i=0; i<arrayLength; i++) {
	cummulative += abs_x[i];
	if ((cummulative/sum) >= qs[q]){
	  argmax = i;
	  break;
	}
	
      }
      result = (argmax+1.0)/arrayLength;
    }
    results[q].value = result;
    
  }

}
