#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double maximum(double array[], int arrayLength) {
  /**************************************************************************
  Function: maximum
  Return Type: double
  ***************************************************************************
  * Calculate the maximum of the time series.
  **************************************************************************/
  double maximum, result;

  /* Calculate maximum */
  maximum = gsl_stats_max(array, 1, arrayLength);

  return maximum;
}
