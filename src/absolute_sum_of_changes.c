#include "etu.h"
#include "etumath.h"

double absolute_sum_of_changes(double array[], int arrayLength) {
  /**************************************************************************
  Function: absolute_sum_of_changes
  Return Type: double
  ***************************************************************************
  * Calculates the sum of the absolute value of the difference between
  * consequtive values in the time series
  **************************************************************************/
  double x[arrayLength], result;

  /* Calculate the absolute value of the difference between 
   * subsequent values in array and sum them up */
  abs_diff(x, array, arrayLength);
  result = KahanSum(x, arrayLength-1);
 
  return result;
}
