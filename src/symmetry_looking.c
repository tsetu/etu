#include <string.h>
#include <gsl/gsl_statistics_double.h>
#include <math.h>
#include "etu.h"
#include "utils.h"

void symmetry_looking(double array[], int arrayLength,
		      double rs[], int rLength,
		      ETU_RETURN results[]) {
  /**************************************************************************
  Function: symmetry_looking
  Return Type: feature_return
  ***************************************************************************
  * Calculate how symmetric the time series is by checking
  * if the absolute value of the difference between the mean and median
  * is smaller than r times the range of the time series.
  **************************************************************************/
  double sorted[arrayLength];
  double mean, median, max, min;
  double mean_median_difference, max_min_difference;
  int p;

  /* Calculate the mean, median, min and max of array */
  mean = gsl_stats_mean(array, 1, arrayLength);
  min = gsl_stats_min(array, 1, arrayLength);
  max = gsl_stats_max(array, 1, arrayLength);

  memcpy(sorted, array, arrayLength*sizeof(double));
  qsort(sorted, arrayLength, sizeof(double), doublesortcmp);
  median = gsl_stats_median_from_sorted_data(sorted, 1, arrayLength);

  /* Calculate intermediate steps */
  mean_median_difference = fabs(mean - median);
  max_min_difference = max - min;

  /* Iterate over the rs */
  for (p=0; p<rLength; p++) {
    char fmtstr[FMTSTRSIZE];
    
    /* Create the key */
    create_fmtstr(fmtstr, rs[p]);
    int ret = snprintf(results[p].name, NAME_LENGTH, fmtstr, rs[p]);

    /* Create the value */
    results[p].value = (mean_median_difference < rs[p]*max_min_difference);
  }

}
