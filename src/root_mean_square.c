#include <gsl/gsl_statistics_double.h>
#include <math.h>
#include "etu.h"
#include "utils.h"

double root_mean_square(double array[], int arrayLength) {
  /**************************************************************************
  Function: root_mean_square
  Return Type: double
  ***************************************************************************
  * Calculate the root mean square of the time series.
  **************************************************************************/
  double square[arrayLength];
  int parse_res, i;
  double mean, result;

  /* Calculate square of array */
  for (i=0; i<arrayLength; i++) {
    square[i] = array[i]*array[i];
  }

  /* Calculate mean */
  mean = gsl_stats_mean(square, 1, arrayLength);

  /* Calculate the root mean square */
  result = sqrt(mean);

  return result;
}
