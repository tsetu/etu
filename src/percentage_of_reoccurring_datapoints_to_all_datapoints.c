#include "etu.h"
#include "utils.h"

double percentage_of_reoccurring_datapoints_to_all_datapoints(double array[],
							      int arrayLength) {
  /**************************************************************************
  Function: percentage_of_reoccurring_datapoints_to_all_datapoints
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of values of time series that occur more 
  * than once
  **************************************************************************/
  int i, j, reoccuring_values, counts;
  double result;

  /* Calculate the number of reoccuring values */
  reoccuring_values = 0;
  for (i=0; i<arrayLength; i++) {
    counts = 1;
    for (j=0; j<arrayLength; j++) {
      if (array[i] == array[j]) {
	if (i < j) {
	  counts++;
	} else if (i > j) {
	  break;
	}
      }
    }
    if (counts > 1) {
      reoccuring_values += counts;
    }
  }
  
  result = (double)reoccuring_values / arrayLength;
  return result;
}
