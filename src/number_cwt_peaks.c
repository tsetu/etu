#include "etu.h"
#include "utils.h"
#include "etumath.h"

int number_cwt_peaks(double array[], int arrayLength, int n) {
  /**************************************************************************
  Function: number_cwt_peaks
  Return Type: int
  ***************************************************************************
  * Using a ricker wavelet in a continuous wavelet transform to
  * identify peaks which occur at enough widths and with high enough
  * signal to noise ratios.
  **************************************************************************/
  int peaks;
 
  /* Calculate number of peaks */
  peaks = find_peaks_cwt(array, arrayLength, n);

  return peaks;
}
