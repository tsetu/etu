#include "etu_intel.h"
#if defined USE_INTEL
#include <mkl.h>
#else
#include <string.h>
#include <gsl/gsl_block_double.h>
#include <gsl/gsl_vector_double.h>
#include <gsl/gsl_blas.h>
#endif
#include "etu.h"
#include "utils.h"

double energy(double array[], int arrayLength) {
  /**************************************************************************
  Function: abs_energy
  Return Type: double
  ***************************************************************************
  * Calculate the sum of the squares (or energy)
  **************************************************************************/
  double result;
  
  /* The energy of a time series is the sum of the squares of the
   * series, or the dot product */
#if defined USE_INTEL
  result = cblas_ddot(arrayLength, array, 1, array, 1);
#else
  gsl_block blk;
  gsl_vector x;
  double data[arrayLength];

  memcpy(data, array, sizeof(double)*arrayLength);
  create_vector(&x, &blk, data, arrayLength);
  gsl_blas_ddot(&x, &x, &result);
#endif
  return result;
}
