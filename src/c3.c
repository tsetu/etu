#include "etu.h"
#include "utils.h"

double c3(double array[], int arrayLength, int lag) {
  /**************************************************************************
  Function: c3
  Return Type: double
  ***************************************************************************
  * Calculate the c3 statistic to measure the nolinearity of the time 
  * series.
  **************************************************************************/
  double result;
  int i;
 
  /* Calculate c3 */
  if (2*lag >= arrayLength)
    return 0;
  
  result = 0;
  for (i=0; i<arrayLength-2*lag; i++) {
    result += array[i+2*lag]*array[i+lag]*array[i];
  }
  result /= (arrayLength - 2.0*lag);
  
  return result;
}
