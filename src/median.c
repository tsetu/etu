#include <string.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double median(double array[], int arrayLength) {
  /**************************************************************************
  Function: median
  Return Type: double
  ***************************************************************************
  * Calculate the median of the time series.
  **************************************************************************/
  double sorted[arrayLength];
  double median, result;

  /* Calculate median */
  memcpy(sorted, array, arrayLength*sizeof(double));
  qsort(sorted, arrayLength, sizeof(double), doublesortcmp);
  median = gsl_stats_median_from_sorted_data(sorted, 1, arrayLength);

  return median;
}
