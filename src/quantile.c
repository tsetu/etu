#include <string.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double quantile(double array[], int arrayLength, double q) {
  /**************************************************************************
  Function: quantile
  Return Type: double
  ***************************************************************************
  * Calculate the quantile q of the time series.
  **************************************************************************/
  double sorted[arrayLength], result;

  /* Check that q is between 0 and 1 */
  if ((q < 0) || (q > 1)) {
    result = GSL_NAN;
  } else {
    /* Calculate the quantile given by q */
    memcpy(sorted, array, arrayLength*sizeof(double));
    qsort(sorted, arrayLength, sizeof(double), doublesortcmp);
    result = gsl_stats_quantile_from_sorted_data(sorted, 1, arrayLength, q);
  }

  return result;
}
