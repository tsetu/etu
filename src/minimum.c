#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double minimum(double array[], int arrayLength) {
  /**************************************************************************
  Function: minimum
  Return Type: double
  ***************************************************************************
  * Calculate the minimum of the time series.
  **************************************************************************/
  double minimum, result;

  /* Calculate minimum */
  minimum = gsl_stats_min(array, 1, arrayLength);

  return minimum;
}
