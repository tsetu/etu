#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_math.h>
#include "etu.h"
#include "utils.h"

double first_location_of_minimum(double array[], int arrayLength) {
  /**************************************************************************
  Function: first_location_of_minimum
  Return Type: double
  ***************************************************************************
  * Calculate the percentage of the whole time series where the minimum
  * value of the series first occurs.
  **************************************************************************/
  gsl_block blk;
  gsl_vector x;
  int argmin;
  double result;

  /* Get the argmin of the vector */
  argmin = gsl_stats_min_index(array, 1, arrayLength);

  if (arrayLength > 0)
    result = (double)argmin / arrayLength;
  else
    return GSL_NAN;

  return result;
}
