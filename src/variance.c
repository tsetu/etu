#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double variance(double array[], int arrayLength) {
  /**************************************************************************
  Function: variance
  Return Type: double
  ***************************************************************************
  * Calculate the variance of the time series.
  **************************************************************************/
  double var, result;

  /* Calculate variance */
  var = gsl_stats_variance(array, 1, arrayLength);
  /* var = ((arrayLength-1.0)/arrayLength)*var; */

  return var;
}
