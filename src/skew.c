#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"

double skew(double array[], int arrayLength) {
  /**************************************************************************
  Function: skew
  Return Type: double
  ***************************************************************************
  * Calculate the skew of the time series.
  **************************************************************************/
  double skew;

  /* Calculate skew */
  /* NB this differs from tsfresh as Python uses 
   * the adjusted Fisher-Pearson standardized momen coefficient */
  skew = gsl_stats_skew(array, 1, arrayLength);

  return skew;
}
