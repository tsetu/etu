#include <math.h>
#include <gsl/gsl_histogram.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

double binned_entropy(double array[], int arrayLength, int max_bins) {
  /**************************************************************************
  Function: binned_entropy
  Return Type: double
  ***************************************************************************
  * Calculates the binned entropy by binning the time series into max_bins 
  * bins and calculates the entropy of the bins.
  **************************************************************************/
  gsl_histogram hist;
  double range[max_bins], bin[max_bins+1];
  int i;
  double sum, result;
  
  /* create the histogram from the array */
  hist.n = (size_t)max_bins;
  hist.range = range;
  hist.bin = bin;
  histogram(&hist, array, arrayLength, max_bins);

  /* Calculate sum of the probs*log(probs) */
  sum = 0;
  for (i = 0; i<hist.n; i++) {
    double prob = hist.bin[i] / arrayLength;
    //double prob = .5;
    if (prob != 0.0) {
      sum += prob * log(prob);
    }
  }
  result = -sum;

  return result;
}
