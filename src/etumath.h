#include <gsl/gsl_histogram.h>

typedef struct RIDGE
{
  int peaks;
  int gap;
  int final;
  int dim[1024];
  int index[1024];
} RIDGE;

int crossing(double data[], int dataLength, double value);

void diff(double x[], double data[], int length);

void abs_diff(double x[], double data[], int length);

double KahanSum(double data[], int length);

void histogram(gsl_histogram *hist, double data[],
	       int dataLength, int max_bins);

int isclose(double a, double b);

double tsmode(double data[], int dataLength);

int calculate_fft_coeff(double data[], int dataLength,
			double realCoef[], double imagCoeff[]);

void calc_fft_abs(double data[], int dataLength,
		  double abs[], double absLength);

double fft_centroid(double data[], int dataLength);

double fft_variance(double data[], int dataLength);

double fft_skew(double data[], int dataLength);

double fft_kurtosis(double data[], int dataLength);

void regression(double data[], int dataLength,
		double *slope, double *intercept,
		double *pvalue, double *rvalue,
		double *stderr);

double autocorr(double data[], int dataLength, int lag);

void pacf(double data[], int dataLength, int maxlag, double pacfs[]);

void convolve(double data1[], int aLen1, double data2[], int aLen2,
	      double convolution[aLen1]);

void cwt(double data[], int dataLength, int width,
	 double cwtdata[width][dataLength]);

void find_peaks(int dataLength, int width,
		double data[width][dataLength],
		int peaks[width], int peaklocation[width][dataLength]);

int find_ridges(int dataLength, int width,
		double data[width][dataLength],
		RIDGE ridges[dataLength*width]);

int find_peaks_cwt(double data[], int dataLength, int width);

