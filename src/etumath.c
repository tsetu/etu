#include <string.h>
#include <math.h>
#include <gsl/gsl_block_double.h>
#include <gsl/gsl_vector_double.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_statistics_double.h>
#include "utils.h"
#include "etumath.h"

int crossing(double data[], int dataLength, double value) {
  int i;
  int positive, last_value;
  int crossings;

  crossings = 0;
  last_value = 0;
  for (i=0; i<dataLength; i++) {
    if (data[i] > value) {
      positive = 1;
    } else {
      positive = 0;
    }

    if ((i>0) && (positive != last_value)) {
      crossings += 1;
    }

    last_value = positive;
  }
  return crossings;
}
  
void diff(double x[], double data[], int length) {
  int i;

  /* Calculate the difference between subsequent values in list */
  for (i = 1; i<length; i++) {
    x[i-1] = data[i] - data[i-1];
  }

  return;
}

void abs_diff(double x[], double data[], int length) {
  int i;

  /* Calculate the difference between subsequent values in list */
  for (i = 1; i<length; i++) {
    x[i-1] = fabs(data[i] - data[i-1]);
  }

  return;
}

double KahanSum(double data[], int length) {
  double sum = 0;
  double c = 0;
  double y, t;
  int i;

  for (i=0; i<length; i++) {
    y = data[i];
    t = sum + y;
    if (fabs(sum) >= fabs(y)) {
      c += (sum - t) + y;
    } else {
      c += (y - t) + sum;
    }
    sum = t;
  }

  return sum + c;
}

void histogram(gsl_histogram *hist, double data[],
	       int dataLength, int max_bins) {
  /* Calculate the histogram hist from the data in max_bins */
  double min, max, d;
  gsl_block blk;
  gsl_vector x;
  int i;

  /* Convert data to gsl_vector for ease of processing */
  create_vector(&x, &blk, data, dataLength);
  /* Find the minimum and maximum of the vector */
  min = gsl_vector_min(&x);
  max = gsl_vector_max(&x);

  /* Create the histogram bins */
  //gsl_histogram_set_ranges_uniform(hist, min, max);
  d = (max - min) / max_bins;
  (*hist).range[0] = min;
  for (i=1; i<=max_bins; i++) {
    (*hist).range[i] = (*hist).range[i-1]+d;
    (*hist).bin[i] = 0;
  }

  /* Loop over data and update hist for each value */
  for (i=0; i<dataLength; i++) {
    gsl_histogram_increment(hist, data[i]);
    /* NB: GSL assumes all intervals are half open [..)
     * Numpy assumes all but the last are half open,
     * but the last one is closed
     * To make GSL aggree, we count elements that are
     * equal to the maximum element */
    if (data[i] >= (*hist).range[max_bins]) {
      (*hist).bin[max_bins-1] += 1;
    }
  }
  
  return;
}

int isclose(double a, double b) {
  /* Attempt to duplicate numpy.isclose from Python
   * calc abs(a-b) <= (atol + rtol*abs(b) */
  double atol = 1e-8;
  double rtol = 1e-5;

  return (fabs(a-b) <= (atol + rtol*fabs(b)));
}

double tsmode(double data[], int dataLength) {
  int i, j, maxCount;
  int counts[dataLength];
  double mode, sorted[dataLength];

  memcpy(sorted, data, dataLength*sizeof(double));
  qsort(sorted, dataLength, sizeof(double), doublesortcmp);

  for (i=0; i<dataLength; i++) {
    counts[i] = 0;
    for (j=i; j<dataLength; j++) {
      if (sorted[i] == sorted[j]) {
	counts[i]++;
      }
    }
  }
  maxCount = 0;
  mode = 0;
  for (i=0; i<dataLength; i++) {
    if (counts[i] > maxCount) {
      maxCount = counts[i];
      mode = sorted[i];
    }
  }
  return mode;
}

void complex_abs(double real[], double imag[], int length,
		 double abs[], int absLength){
  int i;
  for (i=0; i<absLength; i++) {
    abs[i] = sqrt(real[i]*real[i]+imag[i]*imag[i]);
  }
}

int calculate_fft_coeff(double data[], int dataLength,
			double realCoeffs[], double imagCoeffs[]) {
  int i;
  gsl_fft_real_wavetable *real;
  gsl_fft_real_workspace *work;

  work = gsl_fft_real_workspace_alloc(dataLength);
  real = gsl_fft_real_wavetable_alloc(dataLength);

  gsl_fft_real_transform(data, 1, dataLength, real, work);
  gsl_fft_real_wavetable_free(real);

  gsl_fft_real_workspace_free(work);

  realCoeffs[0] = data[0];
  imagCoeffs[0] = 0.0;
  for (i=1; i<dataLength-i; i++) {
    double realC = data[2*i-1];
    double imagC = data[2*i];
    if (i == dataLength-i) {
      realCoeffs[i] = data[dataLength-1];
      imagCoeffs[i] = 0.0;
    } else {
      realCoeffs[i] = realC;
      imagCoeffs[i] = imagC;
      realCoeffs[dataLength - i] = realC;
      imagCoeffs[dataLength - i] = -imagC;
    }
  }

  if (i == dataLength-i) {
    realCoeffs[i] = data[dataLength-1];
    imagCoeffs[i] = 0.0;
  }
  
  return 1;
}

void calc_fft_abs(double data[], int dataLength,
		  double abs[], double absLength) {
  double realCoeffs[dataLength], imagCoeffs[dataLength];
  
  calculate_fft_coeff(data, dataLength, realCoeffs, imagCoeffs);

  complex_abs(realCoeffs, imagCoeffs, dataLength, abs, absLength);

}

double fft_moment(double data[], int dataLength, int moment) {
  double sum, dot;
  int i;

  sum = 0;
  dot = 0;
  for (i=0; i<dataLength; i++) {
    int j, m=1;
    
    for (j=0; j<moment; j++) {
      m *= i;
    }
    dot += data[i]*m;
      
    sum += data[i];
  }
  
  return dot/sum;
}

double fft_centroid(double data[], int dataLength) {
  return fft_moment(data, dataLength, 1);
}

double fft_variance(double data[], int dataLength) {
  double cent = fft_centroid(data, dataLength);
  double moment2 = fft_moment(data, dataLength, 2);
  return moment2 - cent*cent;
}

double fft_skew(double data[], int dataLength) {
  double cent = fft_centroid(data, dataLength);
  double var = fft_variance(data, dataLength);
  double moment3 = fft_moment(data, dataLength, 3);
  if (var < 0.5) {
    return GSL_NAN;
  } else {
    return (moment3 - 3*cent*var - cent*cent*cent) / sqrt(var*var*var);
  }
  return GSL_NAN;
}

double fft_kurtosis(double data[], int dataLength) {
  double cent = fft_centroid(data, dataLength);
  double var = fft_variance(data, dataLength);
  double moment2 = fft_moment(data, dataLength, 2);
  double moment3 = fft_moment(data, dataLength, 3);
  double moment4 = fft_moment(data, dataLength, 4);
  if (var < 0.5) {
    return GSL_NAN;
  } else {
    return (moment4 - 4*cent*moment3 + 6*moment2*cent*cent - 3*cent) / (var*var);
  }
  return GSL_NAN;
}

void regression(double data[], int dataLength,
		double *slope, double *intercept,
		double *pvalue, double *rvalue,
		double *stderr) {
  double x[dataLength];
  double c0, c1, cov00, cov01, cov11, sumsq, tssy;
  int i, dof, F;
  
  tssy = gsl_stats_tss(data, 1, dataLength);
  for (i=0; i<dataLength; i++) {
    x[i] = i;
  }
  
  gsl_fit_linear (x, 1, data, 1, dataLength,
		  intercept, slope, &cov00, &cov01, &cov11, &sumsq);

  /* TODO: Fix stderr, pvalue, rvalue */
  /* SEE: https://stackoverflow.com/questions/5503733/getting-p-value-for-linear-regression-in-c-gsl-fit-linear-function-from-gsl-li */
  *stderr = sqrt(cov00);
  
  dof = dataLength-2;
  *rvalue = 1.0 - sumsq/tssy;

  F = *rvalue*dof/(1-*rvalue);

  *pvalue = 1-gsl_cdf_fdist_P(F, 1, dof);
}

double autocorr(double data[], int dataLength, int lag) {
  double y1, y2;
  int i, length;
  double mean, var, sum_product, result;

  /* Calculate the length of the two partial datas 
   * to be used to generate the autocorrelation */
  length = dataLength - lag;

  /* Calculate the mean and variance of data */
  mean = gsl_stats_mean(data, 1, dataLength);
  var = gsl_stats_variance(data, 1, dataLength);

  /* NB: This disagrees with numpy/tsfresh
   * the var calc defaults to 0 degree of freedom instead of 1
   * or divide by N not N-1
   *
   *
   * To duplicate the numpy/tsfresh result use */
   var = gsl_stats_variance_with_fixed_mean(data, 1, dataLength, mean);
   /**/
  
  /* Populate y1 with the first length of data 
   * and y2 with the last length */
  sum_product = 0;
  for (i=0; i<length; i++) {
    y1 = data[i];
    y2 = data[lag+i];
    sum_product += (y1-mean) * (y2-mean);
  }

  /* Return Null is variance is too small */
  if (gsl_fcmp(var, 0, .5e-9) == 0) {
    result = GSL_NAN;
  }

  result = sum_product / (length*var);
  return result;
}

void pacf(double data[], int dataLength, int maxlag, double pacfs[]) {
  double acf[maxlag], phi[maxlag][maxlag];
  int i, j;

  for (i=0; i<maxlag; i++) {
    double num, den;
    acf[i] = autocorr(data, dataLength, i+1);

    num = acf[i];
    den = 1.0;
    for (j=0; j<i; j++) {
      num -= phi[i-1][j]*acf[i-j-1];
      den -= phi[i-1][j]*acf[j];
    }
    phi[i][i] = num/den;
    pacfs[i] = phi[i][i];

    for (j=0; j<i; j++) {
      phi[i][j] = phi[i-1][j] - phi[i][i]*phi[i-1][i-j-1];
    }
  }
}

void insert_ridge(int value, int data[], int max_element) {
  for (int i=max_element; i>0; i--) {
    data[i] = data[i-1];
  }
  data[0] = value;
}

void convolve(double data1[], int aLen1, double data2[], int aLen2,
	      double convolution[aLen1]) {
  /***************************************************************
   * Calculate the convolution of two 1D datas
   * This calculates the convolution of the same size as
   * data1, so picks the middle aLen1 elements
   **************************************************************/
  int i, j, startidx;

  startidx = ceil(aLen2/2.0)-1;
  for(i=startidx; i<startidx+aLen1; i++) {
    convolution[i-startidx] = 0;
    for(j=0; j<GSL_MIN(i,aLen1); j++) {
      int k=i-j;
      if ((k>=0) && (k<aLen2)) {
	convolution[i-startidx] += data1[j]*data2[k];
      }
    }
  }
}

void ricker(int length, int width, double vector[length]) {
  /***************************************************************
   * Calculate the ricker wavelet coefficients of width width.
   **************************************************************/
  double diff, A, widthsq;
  int i;

  A = 2.0 / (sqrt(3.0*width) * pow(M_PI, .25));
  diff = ((double)length - 1.0)/2.0;
  widthsq = (double)width*width;
  
  for (i=0; i<length; i++) {
    double tmp = (i-diff)*(i-diff);
    double mod = 1 - (tmp / widthsq);
    double gauss = exp(-tmp / (2.0*widthsq));

    vector[i] = A*mod*gauss;
  }
}

void cwt(double data[], int dataLength, int width,
	 double cwtarray[width][dataLength]) {
  /***************************************************************
   * Calculate the continuous wavelet transform of data
   * using the rickert transform for all widths from
   * 1 to width
   **************************************************************/
  int w;

  for (w=0; w<width; w++) {
    int N, i;
    
    N = GSL_MIN(10*(w+1), dataLength);
    double rick[N];
    ricker(N, w+1, rick);

    double tmpcwt[dataLength];
    convolve(data, dataLength, rick, N, tmpcwt);
    for (i=0; i<dataLength; i++) {
      cwtarray[w][i] = tmpcwt[i];
    }

  }
  return;
}

int find_1d_peaks(int dataLength, double data[dataLength], int width,
		  int peaklocation[dataLength]) {
  /***************************************************************
   * Identify the peaks that are maximum of elements withing
   * width of either side.
   **************************************************************/
  int tmppeaks[dataLength], i, j, supported_peak;

   /* Calculate number of peaks */
  int peaks = 0;

  for (i=width; i < dataLength-width; i++) {
    int start_idx = GSL_MAX(0, i-width);
    int end_idx = GSL_MIN(i+width, dataLength);
    supported_peak = 1;
    for (j=start_idx; j <= end_idx; j++) {
      if ((i!=j) && (data[j] >= data[i])) {
	/* Not supported */
	supported_peak = 0;
	break;
      }
    }
    if (supported_peak) {
      tmppeaks[peaks] = i;
      peaks += 1;
    }
  }
  for (i=0; i<peaks; i++) {
    peaklocation[i] = tmppeaks[i];
  }
  
  return peaks;
}

void find_peaks(int dataLength, int width,
		double data[width][dataLength],
		int peaks[width], int peaklocation[width][dataLength]) {
  /***************************************************************
   * Using the 1d find peaks routine, return the number of peaks
   * in each row in data and the indicies of those datas
   **************************************************************/
  double ridge_lines[width][dataLength];
  int w;

  for (w=0; w<width; w++) {
    int pl[dataLength];
    peaks[w] = find_1d_peaks(dataLength, data[w], 1, pl);
    for (int i=0; i<dataLength; i++){
      if (i<peaks[w]){
	peaklocation[w][i] = pl[i];
      } else {
	peaklocation[w][i] = -1;
      }
    }
  }
}

int find_ridges(int dataLength, int width,
		double data[width][dataLength],
		RIDGE ridges[dataLength*width]) {
  /***************************************************************
   * Identify ridges from the 2D map of peaks using
   * CWT ricker transform
   * Du, P., Kibbe, W.A. and Lin, S.M. (2006) 
   * Improved peak detection in mass spectrum by incorporating 
   * continuous wavelet transform-based pattern matching, 
   * Bioinformatics, 22, 2059-2065. 
   **************************************************************/
  int peaks[width], peaklocation[width][dataLength];
  int i, w, p, num_ridges=0;
  
  /* Find local peaks */
  find_peaks(dataLength, width, data, peaks, peaklocation);
  
  /* Find maximum row with a peak to start with */
  int startrow = -1;
  for (i=0; i<width; i++) {
    if (peaks[i] > 0)
      startrow = i;
  }
  if (startrow == -1) {
    return 0;
  }
  /* Create Initial Ridges */
  for (i=0; i<peaks[startrow]; i++) {
    ridges[i].dim[0] = startrow;
    ridges[i].index[0] = peaklocation[startrow][i];
    ridges[i].peaks = 1;
    ridges[i].gap = 0;
    ridges[i].final = 0;
    num_ridges += 1;
  }
  
  
  /* Walk down the list of peaks looking to map peaks to existing ridges */
  for (w=startrow-1; w>=0; w--) {
    double max_distance = (w+1.0)/4.0;
    
    /* Increment gap for each ridge we know about 
     * will reset latter if necessary */
    for (i=0; i<num_ridges; i++) {
      if (!ridges[i].final) {
	ridges[i].gap++;
      }
    }
      
    /* Compare peak p to existing ridges to see 
     * if it is within max_distance of any of those
     * ridges */
    for (p=0; p<peaks[w]; p++){
      int closest = -1;
      double closest_diff = dataLength;
      for (i=0; i<num_ridges; i++) {
	if (!ridges[i].final){
	  double diff = fabs((double)peaklocation[w][p] - ridges[i].index[0]);
	  if (diff < closest_diff) {
	    closest = i;
	    closest_diff = diff;
	  }
	}
      }
      if (closest_diff <= max_distance) {
	/* This ridge was close enough, add to existing ridge */
	insert_ridge(w, ridges[closest].dim, ridges[closest].peaks);
	insert_ridge(peaklocation[w][p], ridges[closest].index, ridges[closest].peaks);
	ridges[closest].peaks++;
	ridges[closest].gap = 0;
      } else {
	/* No close ridge found, add new ridge */
	ridges[num_ridges].dim[0] = w;
	ridges[num_ridges].index[0] = peaklocation[w][p];
	ridges[num_ridges].peaks = 1;
	ridges[num_ridges].gap = 0;
	ridges[num_ridges].final = 0;
	num_ridges += 1;
      }
    }

    /* Mark any ridge with gap over 1 as final */
    for (i=0; i<num_ridges; i++) {
      if (ridges[i].gap > 1) {
	ridges[i].final = 1;
      }
    }
  }
  return num_ridges;
}

int find_number_ridges(int dataLength, int width,
		       double data[width][dataLength], 
		       int number_ridges,
		       RIDGE ridges[dataLength*width]) {
  int number_peaks, i, j;
  int num_points = dataLength;
  int window_size = ceil(num_points/20.0);
  int min_length = ceil(width/4.0);
  int min_snr = 1;
  int noise_pct = 10;
  int hf_window = window_size / 2;
  int odd = window_size % 2;
  int size = 2*hf_window + odd;
  double noise[dataLength] , sorted[size];

  for (i=0; i<dataLength; i++) {
    int window_start = GSL_MAX(i-hf_window, 0);
    int window_end = GSL_MIN(i+hf_window+odd, dataLength);
    size = window_end - window_start;
    double idx = (noise_pct/100.0)*(size-1.0);
    int int_idx = floor(idx);
    double sum = 0;
    double sumw = 0;
    int inc = 2;
    double weight = int_idx+1-idx;
    if ((double)int_idx == idx) {
      inc = 1;
      weight = 1;
    }
    for (j=0; j<size; j++) {
      sorted[j] = data[0][window_start+j];
    }
    qsort(sorted, size, sizeof(double), doublesortcmp);

    for (j=int_idx; j<int_idx+inc; j++) {
      sum += sorted[j]*weight;
      sumw += weight;
      weight = idx-int_idx;
    }    
    noise[i] = sum/sumw;
  }

  number_peaks = 0;
  for (i=0; i<number_ridges; i++) {
    int w = ridges[i].dim[0];
    int p = ridges[i].index[0];
    double snr = fabs(data[w][p]/noise[p]);
    if (ridges[i].peaks < min_length) {
    } else if (snr >= min_snr) {
      number_peaks++;
    }
  }
  
  return number_peaks;
}

int find_peaks_cwt(double data[], int dataLength, int width) {
  double cwtarray[width][dataLength];
  RIDGE ridges[dataLength*width];
  int number_ridges, number_peaks;

  cwt(data, dataLength, width, cwtarray);

  number_ridges = find_ridges(dataLength, width, cwtarray, ridges);

  number_peaks = find_number_ridges(dataLength, width, cwtarray,
				    number_ridges, ridges);


  return number_peaks;
}
