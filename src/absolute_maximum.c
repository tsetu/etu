#include <math.h>
#include <gsl/gsl_statistics_double.h>
#include "etu.h"
#include "utils.h"
#include "etumath.h"

double absolute_maximum(double array[], int arrayLength) {
  /**************************************************************************
  Function: absolute_maximum
  Return Type: double
  ***************************************************************************
  * Calculates the maximum of the absolute values of the time series
  **************************************************************************/
  double abs_x[arrayLength], result;
  int i;

  /* Calculate the absolute value of the time series */
  for (i=0; i<arrayLength; i++) {
    abs_x[i] = fabs(array[i]);
  }
  
  /* Find the maximum value */
  result = gsl_stats_max(abs_x, 1, arrayLength);
  return result;
}
