#include "etu.h"
#include "utils.h"

double time_reversal_asymmetry_statistic(double array[], int arrayLength,
					 int lag) {
  /**************************************************************************
  Function: time_reversal_asymmetry_statistic
  Return Type: double
  ***************************************************************************
  * Calculates the time reversal asymmetry statistic.
  **************************************************************************/
  double result;
  int i;
 
  /* Calculate statistic */
  if (2*lag >= arrayLength)
    result = 0;
  else {
    result = 0;
    for (i=0; i<arrayLength-2*lag; i++) {
      result += array[i+2*lag]*array[i+2*lag]*array[i+lag]
	- array[i+lag]*array[i]*array[i];
    }
    result /= (arrayLength - 2.0*lag);
  }
  
  return result;
}
