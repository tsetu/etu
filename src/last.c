#include "etu.h"
#include "utils.h"

double last(double array[], int arrayLength) {
  /**************************************************************************
  Function: last
  Return Type: double
  ***************************************************************************
  * Return the last or most recent value in the time series.
  **************************************************************************/

  return array[arrayLength-1];
}
